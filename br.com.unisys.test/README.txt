------------------------------------------------------------
	UNISYS-TEST API documentation version v2 /Unisys
------------------------------------------------------------

Instru��es para execu��o do app br.com.unisys.test

- Importar o pacote na IDE Java de sua prefer�ncia. O app foi desenvolvido na IDE Eclipse via projeto Maven.

- Executar a classe "br.com.unisys.test\src\main\java\br\com\unisys\test\Principal.java" e o app ir� iniciar. O funcionamento � apenas no console(em modo texto), pois conforme orienta��o do teste recebido no e-mail a parte front-end n�o precisava ser desenvolvida.

- Selecione as op��es do menu em ordem crescente, uma de cada vez e aguarde o processamento no console. Algumas msgs de warning do hibernate ir�o aparecer, mas o app continua o seu processamento.
Exemplo do menu:

	"TESTE UNISYS - Autor: Diego Fernandes da Silva
	Selecione uma op��o para execu��o
	1 - Criar Jobs.
	2 - Criar Tasks.
	3 - Listar Jobs.
	4 - Listar Tasks.
	5 - Sair"

- Ap�s executar cada op��o o app exibe a msg exclusiva de cada op��o e termina com a msg:
"Pressione ENTER para continuar..."

- Ao selecionar uma op��o inexistente no menu o app exibe a msg:
"Op��o Inv�lida! Tente novamente."

- Os dados do app s�o registrados no banco SQLite via hibernate no diret�rio raiz do app "./testunisys.db", conforme configura��o "javax.persistence.jdbc.url" no arquivo "br.com.unisys.test\src\main\resources\META-INF\persistence.xml"

- O app est� sendo disponibilizado com o arquivo "./testunisys.db" j� salvo na pasta raiz, podendo ser conferido o seu conte�do com o software "DB Browser (SQLite)", dispon�vel para download em:
https://sqlitebrowser.org/dl/
https://download.sqlitebrowser.org/DB.Browser.for.SQLite-3.11.2-win64.zip

- O arquivo "./testunisys.db" pode ser deletado da pasta raiz do app, pois ao executar o app o mesmo ir� cri�-lo novamente inserindo os registros no banco.

Atenciosamente,
Diego Fernandes da Silva