package br.com.unisys.test;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import br.com.unisys.test.entity.Task;

public class VerTask {
	
	protected void VisualizandoTask () {				
		EntityManager em = Persistence.createEntityManagerFactory("testunisysPU")
				   .createEntityManager();
		em.getTransaction().begin();
		
		List<Task> lista = em.createQuery("FROM Task", Task.class).getResultList();
		
		System.out.println("LISTA DE TASKS \n");
		
		if (lista.isEmpty() ) {
			System.out.println("Nenhuma Task encontrado. A lista est� vazia!\n");
		}
		else {
			for (Task obj : lista ) {
				System.out.println("ID: " + obj.getId() + ". NOME: " + obj.getName() + ". PESO: " + obj.getWeight()
				+ ". STATUS: " + obj.isCompleted() + ". DATA DE CRIA��O: " + obj.getCreatedAt() );
			}
		}
		em.close();
	}
}
