package br.com.unisys.test;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import br.com.unisys.test.entity.Job;

public class CriaJob {
	
	protected void NovoJob (String nome, boolean status, String tarefas, int parentesco) {
		EntityManager em = Persistence.createEntityManagerFactory("testunisysPU")
				   .createEntityManager();
		em.getTransaction().begin();
		
		Job criandoJob = new Job();
		criandoJob.setName(nome);
		criandoJob.setActive(status);
		criandoJob.setTasks(tarefas);
		criandoJob.setParentJob(parentesco);
		
		em.persist(criandoJob);
		em.getTransaction().commit();
		em.close();
	}
}
