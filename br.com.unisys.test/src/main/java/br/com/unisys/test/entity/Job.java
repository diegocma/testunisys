package br.com.unisys.test.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Job {
	
	@Id
	@GeneratedValue
	private long id;
	
	private String name;
	private boolean active;
	private String tasks;
	private int parentJob;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public String getTasks() {
		return tasks;
	}
	public void setTasks(String tasks) {
		this.tasks = tasks;
	}
	public int getParentJob() {
		return parentJob;
	}
	public void setParentJob(int parentJob) {
		this.parentJob = parentJob;
	}	
}
