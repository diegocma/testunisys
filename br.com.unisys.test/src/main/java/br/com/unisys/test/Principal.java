package br.com.unisys.test;

import java.io.IOException;
import java.util.Scanner;

public class Principal {

	public static void main(String[] args) throws IOException {
		
		Scanner s = new Scanner(System.in);
		int controle = 0;
		
		do {
			System.out.println("TESTE UNISYS - Autor: Diego Fernandes da Silva \n");
			System.out.println("Selecione uma op��o para execu��o.\n"
							 + "1 - Criar Jobs.\n"
							 + "2 - Criar Tasks.\n"
							 + "3 - Listar Jobs.\n"
							 + "4 - Listar Tasks.\n"
							 + "5 - Sair");
			controle = s.nextInt();
			
			switch(controle) {
			
			case 1:
				{
					AddJT novo = new AddJT();
					novo.AddJobs();
					System.out.println("\nJobs criados com sucesso!");
					pause();				
					break;
				}
			case 2:
				{
					AddJT novo2 = new AddJT();
					novo2.AddTasks();
					System.out.println("\nTasks criados com sucesso!");
					pause();				
					break;
				}
			case 3:
				{
					VerJob verj = new VerJob();
					verj.VisualizandoJob();
					pause();				
					break;
				}
			case 4:
				{
					VerTask vert = new VerTask();
					vert.VisualizandoTask();
					pause();				
					break;
				}
			case 5:
				break;
				
			default:
				{
					System.out.println("\nOp��o Inv�lida! Tente novamente.");
			        pause();
			        break;
				}
			
			} //Fim Switch Case

		} while(controle != 5);
		
		s.close(); //Fechando Scanner
	}
	
	private static void pause() throws IOException {
		System.out.println("Pressione ENTER para continuar...\n");
		System.in.read();
	}

}
