package br.com.unisys.test;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import br.com.unisys.test.entity.Task;

public class CriaTask {
	
	protected void NovaTask (String nome, long peso, boolean status, String criadoEm) {
		EntityManager em = Persistence.createEntityManagerFactory("testunisysPU")
				   .createEntityManager();
		em.getTransaction().begin();
		
		Task criandoTask = new Task();
		criandoTask.setName(nome);
		criandoTask.setWeight(peso);
		criandoTask.setCompleted(status);
		criandoTask.setCreatedAt(criadoEm);
		
		em.persist(criandoTask);
		em.getTransaction().commit();
		em.close();	
	}
}
