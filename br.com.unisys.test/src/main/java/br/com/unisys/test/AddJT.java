package br.com.unisys.test;

public class AddJT {
	
	protected void AddJobs() {
		//Criando os Jobs
		CriaJob criando = new CriaJob();
		criando.NovoJob("Job 1", true, "Trabalho do Diego", 0); //Se o parentesco for 0 indica que o Job n�o tem relacionamento/parentesco.

		CriaJob criando2 = new CriaJob();
		criando2.NovoJob("Job 2", false, "Trabalho cancelado!", 1);

		CriaJob criando3 = new CriaJob();
		criando3.NovoJob("Job 3", true, "Trabalho pendente de respons�vel", 1);
		
		CriaJob criando4 = new CriaJob();
		criando4.NovoJob("Job 3", true, "Trabalho pendente de respons�vel", 1);
		
		CriaJob criando5 = new CriaJob();
		criando5.NovoJob("Job 4", false, "Trabalho developer back-end", 2);
		
		CriaJob criando6 = new CriaJob();
		criando6.NovoJob("Job 2", false, "Trabalho cancelado!", 3);
		
		CriaJob criando7 = new CriaJob();
		criando7.NovoJob("Job 5", true, "Trabalho developer front-end", 3);
	}
	
	protected void AddTasks() {
		//Criando as Tasks
		CriaTask tarefa = new CriaTask();
		tarefa.NovaTask("Importar os arquivos na nuvem", 5, true, "2019-08-21");

		CriaTask tarefa2 = new CriaTask();
		tarefa2.NovaTask("Implementar novo app", 8, true, "2019-07-01");

		CriaTask tarefa3 = new CriaTask();
		tarefa3.NovaTask("Testar nova aplica��o web", 3, false, "2018-12-02");
		
		CriaTask tarefa4 = new CriaTask();
		tarefa4.NovaTask("Curso JSON e JQUERY", 20, true, "2019-09-02");
		
		CriaTask tarefa5 = new CriaTask();
		tarefa5.NovaTask("Fazer deploy na nuvem", 1, false, "2019-10-20");
	}

}
