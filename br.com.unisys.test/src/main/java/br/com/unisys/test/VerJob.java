package br.com.unisys.test;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import br.com.unisys.test.entity.Job;

public class VerJob {
	
	protected void VisualizandoJob () {				
		EntityManager em = Persistence.createEntityManagerFactory("testunisysPU")
				   .createEntityManager();
		em.getTransaction().begin();
		
		List<Job> lista = em.createQuery("FROM Job", Job.class).getResultList();
		
		System.out.println("LISTA DE JOBS \n");
		
		if (lista.isEmpty() ) {
			System.out.println("Nenhum Job encontrado. A lista est� vazia!\n");
		}
		else {
			for (Job obj : lista ) {
				System.out.println("ID: " + obj.getId() + ". NOME: " + obj.getName() + ". STATUS: " + obj.isActive()
				+ ". DESCRI��O: " + obj.getTasks() + ". PARENTESCO DO JOB: " + obj.getParentJob() );
			}
		}
		em.close();
	}
}
