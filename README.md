------------------------------------------------------------
	UNISYS-TEST API documentation version v2 /Unisys
------------------------------------------------------------

Instruções para execução do app br.com.unisys.test

- Importar o pacote na IDE Java de sua preferência. O app foi desenvolvido na IDE Eclipse via projeto Maven.

- Executar a classe "br.com.unisys.test\src\main\java\br\com\unisys\test\Principal.java" e o app irá iniciar. O funcionamento é apenas no console(em modo texto), pois conforme orientação do teste recebido no e-mail a parte front-end não precisava ser desenvolvida.

- Selecione as opções do menu em ordem crescente, uma de cada vez e aguarde o processamento no console. Algumas msgs de warning do hibernate irão aparecer, mas o app continua o seu processamento.
Exemplo do menu:

	"TESTE UNISYS - Autor: Diego Fernandes da Silva
	Selecione uma opção para execução
	1 - Criar Jobs.
	2 - Criar Tasks.
	3 - Listar Jobs.
	4 - Listar Tasks.
	5 - Sair"

- Após executar cada opção o app exibe a msg exclusiva de cada opção e termina com a msg:
"Pressione ENTER para continuar..."

- Ao selecionar uma opção inexistente no menu o app exibe a msg:
"Opção Inválida! Tente novamente."

- Os dados do app são registrados no banco SQLite via hibernate no diretório raiz do app "./testunisys.db", conforme configuração "javax.persistence.jdbc.url" no arquivo "br.com.unisys.test\src\main\resources\META-INF\persistence.xml"

- O app está sendo disponibilizado com o arquivo "./testunisys.db" já salvo na pasta raiz, podendo ser conferido o seu conteúdo com o software "DB Browser (SQLite)", disponível para download em:
https://sqlitebrowser.org/dl/
https://download.sqlitebrowser.org/DB.Browser.for.SQLite-3.11.2-win64.zip

- O arquivo "./testunisys.db" pode ser deletado da pasta raiz do app, pois ao executar o app o mesmo irá criá-lo novamente inserindo os registros no banco.

Atenciosamente,
Diego Fernandes da Silva